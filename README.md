Esercizio di recruiting Mobile

Realizzare un’app Android che visualizzi Programming quotes e immagini di cani o gatti

Flusso di utilizzo  
     •Apro l’app per la prima volta e mi chiede se preferisco “cani” o “gatti”  
     •Scelgo uno o l’altro  
     •Visualizzo una lista di “Programming quotes”casuali  
     •Se faccio tap su un quote,si apre il dettaglio della quote dove si vedono:  
        - l’immagine casuale di un cane o un gatto,in base a quanto scelto inizialmente  
        - il testo della quote  
     •All’avvio successivo dell’app,rimane memorizzata la scelta “cani” o “gatti"  
    
Bonus  
     •Funzionalità aggiuntiva: poter cambiare in app la scelta “cani” o ”gatti”  
     •Uso di test automatici  

API  
     •Programming quotes: https://programming-quotes-api.herokuapp.com/  
     •Immagini di gatti:https://aws.random.cat/meow  
     •Immagini di cani:https://random.dog/woof.json  