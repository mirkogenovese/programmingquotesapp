package com.example.programmingquotesapp;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class AnimalSelectionInstrumentedTest {

    @Rule
    public ActivityTestRule<AnimalSelectionActivity> mActivityRule =
            new ActivityTestRule<>(AnimalSelectionActivity.class);

    /**
     * Test that verify if the dog button actually change the animal preference
     */
    @Test
    public void ensureDogButtonWork() {
        onView(withId(R.id.btn_dog)).perform(click());

        String text = "Animal selected: Dog";
        onView(withId(R.id.tx_animal)).check(matches(withText(text)));
    }

    /**
     * Test that verify if the cat button actually change the animal preference
     */
    @Test
    public void ensureCatButtonWork() {
        onView(withId(R.id.btn_cat)).perform(click());

        String text = "Animal selected: Cat";
        onView(withId(R.id.tx_animal)).check(matches(withText(text)));
    }

}