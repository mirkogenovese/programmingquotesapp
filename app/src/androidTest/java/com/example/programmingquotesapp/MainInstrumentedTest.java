package com.example.programmingquotesapp;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class MainInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    /**
     * Test that verify if the menu item for changing animal work
     */
    @Test
    public void ensureChangeAnimalButtonWork() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());

        onView(withText(R.string.change_animal)).perform(click());
        onView(withId(R.id.btn_dog)).check(matches(withText("Dog")));
    }

    /**
     * Test that verify if clicking on a quote open the correct activity
     */
    @Test
    public void ensureTapOnQuoteWork() {
        onView(withId(R.id.recycler_view)).perform(click());
        onView(withId(R.id.img_view)).check(matches(withId(R.id.img_view)));
    }

    /**
     * Test that verify if the loadMore button work correctly
     */
    @Test
    public void ensureLoadMoreQuoteWork() {

        RecyclerView recyclerView  = (RecyclerView) mActivityRule.getActivity().findViewById(R.id.recycler_view);
        int count1 = recyclerView.getChildCount();

        onView(withId(R.id.btn_load_more)).perform(click());

        onView(isRoot()).perform(waitFor(5000));

        int count2 = recyclerView.getChildCount();

        assertThat(count2, greaterThan(count1));
    }

    /**
     * Perform action of waiting for a specific time.
     */
    public static ViewAction waitFor(final long millis) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }

            @Override
            public String getDescription() {
                return "Wait for " + millis + " milliseconds.";
            }

            @Override
            public void perform(UiController uiController, final View view) {
                uiController.loopMainThreadForAtLeast(millis);
            }
        };
    }
}