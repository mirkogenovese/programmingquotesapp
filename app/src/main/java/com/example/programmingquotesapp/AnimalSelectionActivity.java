package com.example.programmingquotesapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.programmingquotesapp.object.Animal;

public class AnimalSelectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal_selection_layout);
    }

    /**
     * Manage the button click and call selected method with the animal index
     * @param v
     */
    public void onClick(View v) {
        int selectedAnimal = -1;
        Animal animal = new Animal();
        switch (v.getId()) {
            case R.id.btn_dog: {
                saveSelectAnimalPreference(animal.getDogCode());
                break;
            }
            case R.id.btn_cat: {
                saveSelectAnimalPreference(animal.getCatCode());
                break;
            }
        }

        returnSelectionToMain();
    }

    /**
     * Save the selected animal in the shared preference
     * @param value animal index
     */
    private void saveSelectAnimalPreference(int value) {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("pref", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("animal_choice", value);
        editor.apply();
    }

    /**
     * Method used to create the MainView
     */
    private void returnSelectionToMain() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
}