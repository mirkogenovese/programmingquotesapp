package com.example.programmingquotesapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.programmingquotesapp.adapter.RecyclerAdapterQuote;
import com.example.programmingquotesapp.object.Animal;
import com.example.programmingquotesapp.object.Quote;
import com.example.programmingquotesapp.network.QuoteDownloader;
import com.example.programmingquotesapp.network.QuoteDownloaderReceiver;
import com.example.programmingquotesapp.network.instance.RetrofitQuoteInstance;
import com.example.programmingquotesapp.network.service.QuoteService;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements QuoteDownloaderReceiver {

    private final int LAUNCH_ANIMAL_SELECTION_ACTIVITY = 0;

    private static List<Quote> quotes;
    private RecyclerAdapterQuote quoteAdapter;
    private Button btnLoadMore;
    private static int currentRequestCount = 0;
    private QuoteService service;

    /**
     * Method used to set the contentView
     * Also check if an animal has been selected. If not, create the AnimalSelectorActivity
     * Otherwise, it call the initializeMain method
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!isNetworkAvailable()) {
            initializeNetworkNotAvailable();
            return;
        }

        if(!isAnimalPreferenceSet()) {
            loadAnimalSelectorActivity();
        }

        initializeMain();
    }

    private boolean isAnimalPreferenceSet() {
        return getAnimalPreference() != -1;
    }

    private int getAnimalPreference() {
        SharedPreferences preference = getSharedPreferences("pref", 0);
        return preference.getInt("animal_choice", -1);
    }

    /**
     *  Method used create the AnimalSelectorActivity
     */
    private void loadAnimalSelectorActivity() {
        Intent intent = new Intent(this, AnimalSelectionActivity.class);
        startActivityForResult(intent, LAUNCH_ANIMAL_SELECTION_ACTIVITY);
    }

    /**
     * Handle the creation of the menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * If the menu item clicked is Change Animal, then call the method for opening the animal selector
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_animal) {
            loadAnimalSelectorActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     *  Method used to initialize the Main activity.
     *  It create the recyclerView and populate it with some quotes
     */
    private void initializeMain() {
        initializeGUI();
        initializeQuotes();
    }

    private void initializeGUI() {
        btnLoadMore = findViewById(R.id.btn_load_more);
        TextView tx_animal = findViewById(R.id.tx_animal);
        Animal selectedAnimal = new Animal(getAnimalPreference());
        String text = getString(R.string.animal_selected, selectedAnimal.getAnimalName(this));
        tx_animal.setText(text);
    }

    private void initializeQuotes() {
        quotes = new ArrayList<>();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        quoteAdapter = new RecyclerAdapterQuote(quotes);
        recyclerView.setAdapter(quoteAdapter);

        loadMoreQuotes(null);
    }

    /**
     * Method used to request more quotes using Tasks
     * @param v
     */
    public void loadMoreQuotes(View v) {

        if(!isNetworkAvailable()) {
            initializeNetworkNotAvailable();
            return;
        }

        btnLoadMore.setEnabled(false);

        for(int i = 0; i < 10; i++) {
            manageRequestCount(1);

            QuoteDownloader.getQuote(getService(), this);
        }
    }

    /**
     * Metod used to update the number of current request
     * @param value value to add to the current request number
     */
    private synchronized void manageRequestCount(int value) {
        currentRequestCount += value;

        if(currentRequestCount == 0) {
            btnLoadMore.setEnabled(true);
        }
    }

    public void addQuote(Quote quote) {
        if(quote == null) {
            return;
        }

        quotes.add(quote);
        quoteAdapter.notifyDataSetChanged();

        manageRequestCount(-1);
    }

    /**
     * method used to check for internet access.
     * Display a toast and disable loadMore button if no connection is found
     * @return if the connection is present or not
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void initializeNetworkNotAvailable() {
        Toast.makeText(getApplicationContext(),R.string.error_connection,Toast.LENGTH_LONG).show();
        if(btnLoadMore != null) {
            btnLoadMore.setEnabled(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LAUNCH_ANIMAL_SELECTION_ACTIVITY) {
            if(resultCode == Activity.RESULT_OK){
                initializeMain();
            }
        }
    }

    @Override
    public void receiveQuote(Quote quote) {
        addQuote(quote);
    }

    @Override
    public void onError() {
        initializeNetworkNotAvailable();
    }

    private QuoteService getService() {
        if(service == null) {
            service = new RetrofitQuoteInstance().getInstance();
        }

        return service;
    }
}