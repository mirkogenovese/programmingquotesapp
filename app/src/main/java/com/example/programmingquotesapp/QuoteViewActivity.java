package com.example.programmingquotesapp;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.programmingquotesapp.object.Animal;
import com.example.programmingquotesapp.object.Image;
import com.example.programmingquotesapp.object.Quote;
import com.example.programmingquotesapp.network.ImageDownloader;
import com.example.programmingquotesapp.network.ImageDownloaderReceiver;
import com.example.programmingquotesapp.network.instance.RetrofitCatInstance;
import com.example.programmingquotesapp.network.instance.RetrofitDogInstance;
import com.example.programmingquotesapp.network.service.ImageService;

public class QuoteViewActivity extends AppCompatActivity implements ImageDownloaderReceiver {

    Quote quote;
    ImageView imageView;
    VideoView videoView;
    ProgressBar progressBar;
    static Animal selectedAnimal;

    /**
     * Method used to set the contentView
     * Also if the intent contain a quote, initialize the view with it, otherwise close the view
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        quote = (Quote) getIntent().getSerializableExtra("quote");
        if(quote == null) {
            finish();
        }

        initializeView(quote);
    }

    /**
     * Method used to set the content of the view with the quote passed
     * Start the task that download a random animal image
     * @param quote quote to display
     */
    public void initializeView(Quote quote) {
        initializeGUI();
        initializeQuote();
        initializeImage();
    }

    private void initializeGUI() {
        imageView = findViewById(R.id.img_view);
        videoView = (VideoView) findViewById(R.id.video_view);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    private void initializeQuote() {
        ((TextView)findViewById(R.id.tx_quote)).setText(quote.en);
        if (quote.author != null && !quote.author.isEmpty()) {
            String author = getString(R.string.by, quote.author);
            ((TextView)findViewById(R.id.tx_author)).setText(author);
        }
        if (quote.rating != null && !quote.rating.isEmpty()) {
            String rating = getString(R.string.rating, quote.rating);
            ((TextView)findViewById(R.id.tx_rating)).setText(rating);
        }
        if (quote.source != null && !quote.source.isEmpty()) {
            String source = getString(R.string.source, quote.source);
            ((TextView)findViewById(R.id.tx_source)).setText(source);
        }
    }

    private void initializeImage() {
        SharedPreferences preferences = getSharedPreferences("pref", 0);
        selectedAnimal = new Animal(preferences.getInt("animal_choice", -1));

        ImageService instance;
        if(selectedAnimal.isDog()) {
            instance = new RetrofitDogInstance().getInstance();
        }
        else {
            instance = new RetrofitCatInstance().getInstance();
        }
        ImageDownloader.getImage(instance, this);
    }

    public void addImage(Image animalImage) {
        if(animalImage == null || animalImage.getUrl().isEmpty()) {
            return;
        }

        String url = animalImage.getUrl();
        if(url.contains(".mp4") || url.contains(".webm")) {
            loadVideo(url);
        }
        else {
            loadImage(url);
        }
    }

    /**
     * load the image in the imageView using glide
     * @param url url of the image
     */
    private void loadImage(String url) {
        imageView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        Glide.with(imageView.getContext()).load(url)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                }).into(imageView);
    }

    /**
     * load the video in the videoView
     * @param url url of the video
     */
    private void loadVideo(String url) {
        videoView.setVisibility(View.VISIBLE);
        videoView.setVideoURI(Uri.parse(url));

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.start();
                mediaPlayer.setLooping(true);
            }
        });
    }

    @Override
    public void receiveImage(Image image) {
        addImage(image);
    }

    @Override
    public void onError() {
        initializeNetworkNotAvailable();
    }

    private void initializeNetworkNotAvailable() {
        Toast.makeText(getApplicationContext(),R.string.error_connection,Toast.LENGTH_LONG).show();
    }
}