package com.example.programmingquotesapp.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.programmingquotesapp.QuoteViewActivity;
import com.example.programmingquotesapp.R;
import com.example.programmingquotesapp.object.Quote;

import java.util.List;

public class RecyclerAdapterQuote extends RecyclerView.Adapter<RecyclerAdapterQuote.QuoteItemHolder> {

    private List<Quote> quoteItems;
    private View inflatedView;

    /**
     * constructor
     * @param items quote list
     */
    public RecyclerAdapterQuote(List<Quote> items) {
        quoteItems = items;
    }

    /**
     * Inflate the view and return the QuoteItemHolder
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public RecyclerAdapterQuote.QuoteItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_quote, parent, false);
        return new QuoteItemHolder(inflatedView);
    }

    /**
     * Call the method to bind the quote on position, with a view
     * @param holder itemHolder containing the view
     * @param position position of the quote
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapterQuote.QuoteItemHolder holder, int position) {
        Quote quote = quoteItems.get(position);
        holder.bindQuoteItem(quote);
    }

    @Override
    public int getItemCount() {
        return quoteItems.size();
    }

    public static class QuoteItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView tx_quote;
        private final TextView tx_author;
        private final TextView tx_rating;
        private Quote quote;
        private View v;

        /**
         * Constructor
         * @param v item view
         */
        public QuoteItemHolder(View v) {
            super(v);
            this.v = v;
            tx_quote = v.findViewById(R.id.tx_quote);
            tx_author = v.findViewById(R.id.tx_author);
            tx_rating = v.findViewById(R.id.tx_rating);

            v.setOnClickListener(this);
        }

        /**
         * set the view content using the quote object passed
         * @param quote quote to display
         */
        public void bindQuoteItem(Quote quote) {
            this.quote = quote;
            tx_quote.setText(quote.en);
            if (quote.author != null && !quote.author.isEmpty()) {
                String author = v.getContext().getString(R.string.by, quote.author);
                tx_author.setText(author);
            }
            if (quote.rating != null && !quote.rating.isEmpty()) {
                String rating = v.getContext().getString(R.string.rating, quote.rating);
                tx_rating.setText(rating);
            }
        }

        /**
         * OnClick over a quote, create the QuoteViewActivity
         * @param v
         */
        @Override
        public void onClick(View v) {
            v.getContext().startActivity((new Intent(v.getContext(), QuoteViewActivity.class)).putExtra("quote", quote));
        }
    }


}
