package com.example.programmingquotesapp.network;

import com.example.programmingquotesapp.object.Image;
import com.example.programmingquotesapp.network.service.ImageService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageDownloader {

    public static void getImage(ImageService retrofitService, ImageDownloaderReceiver receiver) {
        Call<Image> callAsync = retrofitService.getImage();

        callAsync.enqueue(new Callback<Image>() {
            @Override
            public void onResponse(Call<Image> call, Response<Image> response) {
                Image image = response.body();

                receiver.receiveImage(image);
            }

            @Override
            public void onFailure(Call<Image> call, Throwable t) {
                receiver.onError();
            }
        });

    }
}
