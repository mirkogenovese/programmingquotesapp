package com.example.programmingquotesapp.network;

import com.example.programmingquotesapp.object.Image;

public interface ImageDownloaderReceiver {

    void receiveImage(Image image);

    void onError();
}
