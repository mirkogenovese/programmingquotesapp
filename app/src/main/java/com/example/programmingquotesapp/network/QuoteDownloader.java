package com.example.programmingquotesapp.network;

import com.example.programmingquotesapp.object.Quote;
import com.example.programmingquotesapp.network.service.QuoteService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuoteDownloader {

    public static void getQuote(QuoteService retrofitService, QuoteDownloaderReceiver receiver) {
        Call<Quote> callAsync = retrofitService.getQuote();

        callAsync.enqueue(new Callback<Quote>() {
            @Override
            public void onResponse(Call<Quote> call, Response<Quote> response) {
                Quote quote = response.body();

                receiver.receiveQuote(quote);
            }

            @Override
            public void onFailure(Call<Quote> call, Throwable t) {
                receiver.onError();
            }
        });

    }
}
