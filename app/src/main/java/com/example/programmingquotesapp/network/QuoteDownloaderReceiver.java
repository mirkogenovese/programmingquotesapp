package com.example.programmingquotesapp.network;

import com.example.programmingquotesapp.object.Quote;

public interface QuoteDownloaderReceiver {

    void receiveQuote(Quote quote);

    void onError();
}
