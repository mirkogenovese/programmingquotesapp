package com.example.programmingquotesapp.network.instance;

import com.example.programmingquotesapp.network.service.CatService;
import com.example.programmingquotesapp.network.service.ImageService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitCatInstance {

    public ImageService getInstance() {
        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl("https://aws.random.cat/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(CatService.class);
    }
}
