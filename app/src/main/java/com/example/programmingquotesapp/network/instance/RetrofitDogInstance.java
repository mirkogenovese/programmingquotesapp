package com.example.programmingquotesapp.network.instance;

import com.example.programmingquotesapp.network.service.DogService;
import com.example.programmingquotesapp.network.service.ImageService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitDogInstance {

    public ImageService getInstance() {
        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl("https://random.dog/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(DogService.class);
    }
}
