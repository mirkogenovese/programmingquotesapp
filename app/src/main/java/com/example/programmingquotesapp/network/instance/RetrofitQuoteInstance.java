package com.example.programmingquotesapp.network.instance;

import com.example.programmingquotesapp.network.service.QuoteService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitQuoteInstance {

    public QuoteService getInstance() {
        Retrofit retrofit =  new Retrofit.Builder()
                .baseUrl("https://programming-quotes-api.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(QuoteService.class);
    }
}
