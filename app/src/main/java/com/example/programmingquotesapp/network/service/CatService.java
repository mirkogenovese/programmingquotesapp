package com.example.programmingquotesapp.network.service;

import com.example.programmingquotesapp.object.Image;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CatService extends ImageService {

    @Override
    @GET("/meow")
    Call<Image> getImage();
}

