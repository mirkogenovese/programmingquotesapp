package com.example.programmingquotesapp.network.service;

import com.example.programmingquotesapp.object.Image;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DogService extends ImageService {

    @Override
    @GET("/woof.json")
    Call<Image> getImage();
}

