package com.example.programmingquotesapp.network.service;

import com.example.programmingquotesapp.object.Image;

import retrofit2.Call;

public interface ImageService {

    Call<Image> getImage();
}
