package com.example.programmingquotesapp.network.service;

import com.example.programmingquotesapp.object.Quote;

import retrofit2.Call;
import retrofit2.http.GET;

public interface QuoteService {

    @GET("/quotes/random/lang/en")
    Call<Quote> getQuote();
}

