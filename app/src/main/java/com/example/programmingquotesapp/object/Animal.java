package com.example.programmingquotesapp.object;

import android.content.Context;

import com.example.programmingquotesapp.R;

public class Animal {

    final int ANIMAL_DOG = 0;
    final int ANIMAL_CAT = 1;

    public int type;

    public Animal() {}

    public Animal(int type) {
        this.type = type;
    }

    public boolean isDog() {
        return type == ANIMAL_DOG;
    }

    public boolean isCat() {
        return type == ANIMAL_CAT;
    }

    public int getDogCode() {
        return ANIMAL_DOG;
    }

    public int getCatCode() {
        return ANIMAL_CAT;
    }

    public String getAnimalName(Context context) {
        switch (type) {
            case 0: return context.getString(R.string.dog);
            case 1: return context.getString(R.string.cat);
            default: return "";
        }
    }

}
