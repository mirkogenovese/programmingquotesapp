package com.example.programmingquotesapp.object;

public class Image {
    private String url;
    private String file;

    public String getUrl() {
        if(url == null) {
            if(file == null) {
                return "";
            }
            else {
                return file;
            }
        }
        else {
            return url;
        }
    }
}
