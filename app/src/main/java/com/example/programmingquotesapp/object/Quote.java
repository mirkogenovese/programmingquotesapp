package com.example.programmingquotesapp.object;

import java.io.Serializable;

public class Quote implements Serializable {

    public String en;
    public String author;
    public String source;
    public String rating;

}
